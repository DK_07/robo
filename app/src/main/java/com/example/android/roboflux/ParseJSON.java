package com.example.android.roboflux;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ParseJSON extends Activity implements View.OnClickListener{

    private String myJSONString;

    private static final String JSON_ARRAY ="result";
    private static final String NAME = "name";
    private static final String LONGITUDE = "longitude";

    private static final String LATITUDE = "latitude";


    private JSONArray users;

    private int TRACK = 0;

    private EditText editTextName, editTextlatitude, editTextlongitude;

    Button btnPrev;
    Button btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parse_json);

        Intent intent = getIntent();
        myJSONString = intent.getStringExtra(MapActivity.MY_JSON);


        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextlatitude = (EditText) findViewById(R.id.editTextUsername);
        editTextlongitude = (EditText) findViewById(R.id.editTextPassword);

        btnPrev = (Button) findViewById(R.id.buttonPrev);
        btnNext = (Button) findViewById(R.id.buttonNext);

        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        extractJSON();

        this.showData();
    }

    @Override
    public void onClick(View v) {

        if(v == btnNext){
            moveNext();
        }
        if(v == btnPrev){
            movePrev();
        }

    }

    private void extractJSON(){
        try {
            JSONObject jsonObject = new JSONObject(myJSONString);
            users = jsonObject.getJSONArray(JSON_ARRAY);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void moveNext(){
        if(TRACK<users.length()){
            TRACK++;
        }
        showData();
    }

    private void movePrev(){
        if(TRACK>0){
            TRACK--;
        }
        showData();
    }

    private void showData(){
        try {
            JSONObject jsonObject = users.getJSONObject(TRACK);

            editTextName.setText(jsonObject.getString(NAME));
            editTextlatitude.setText(jsonObject.getString(LATITUDE));
            editTextlongitude.setText(jsonObject.getString(LONGITUDE));
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this,e+"",Toast.LENGTH_LONG).show();
        }


    }

}
