package com.example.android.roboflux;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.api.GoogleApiClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class patient extends Activity implements View.OnClickListener{

    private Button buttonDoctor;
    private Button buttonPathalogy;
    private Button buttonPharmacy;

    public String str;

    public static final String MY_JSON = "MY_JSON";

    private static final String JSON_URL = "http://192.168.43.153/webapp/json.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);

        buttonPathalogy = (Button) findViewById(R.id.buttonPathalogy);
        buttonDoctor = (Button) findViewById(R.id.buttonDoctor);
        buttonPharmacy = (Button) findViewById(R.id.buttonPharmacy);
        buttonPathalogy.setOnClickListener(this);
        buttonDoctor.setOnClickListener(this);
        buttonPharmacy.setOnClickListener(this);

    }
    public void patient_pathology(View view){
       //getJSON(JSON_URL);
        Intent intent = new Intent(this, MapActivity.class);
        //intent.putExtra(MY_JSON,str);
        startActivity(intent);

    }
    public void patient_doctor(View view){
       //getJSON(JSON_URL);
        Intent intent = new Intent(this, MapActivity.class);
        //intent.putExtra(MY_JSON,str);
        startActivity(intent);
    }
    public void patient_pharmacy(View view){
     //  getJSON(JSON_URL);
        Intent intent = new Intent(this, MapActivity.class);
       // intent.putExtra(MY_JSON,str);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if(v==buttonDoctor)
            patient_doctor(v);
        else if(v==buttonPathalogy)
            patient_pathology(v);
        else if(v==buttonPharmacy)
            patient_pharmacy(v);
    }

/*    private void getJSON(String url) {
        class GetJSON extends AsyncTask<String, Void, String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(patient.this, "Please Wait...", null, true, true);
            }

            @Override
            protected String doInBackground(String... params) {

                String uri = JSON_URL;

                BufferedReader bufferedReader = null;
                try {
                    URL url = new URL(uri);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();

                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String json;
                    while ((json = bufferedReader.readLine()) != null) {
                        sb.append(json + "\n");
                    }

                    return sb.toString().trim();

                } catch (Exception e) {
                    return null;
                }

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                str=s;
                //textViewJSON.setText(s);
            }


        }

        GetJSON gj = new GetJSON();
        gj.execute(url);
    }
*/
}
